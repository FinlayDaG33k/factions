function GetPlayerFaction(UUID)
  local sql = "SELECT faction_id FROM faction_members WHERE player_uuid = ?";
  local faction = ExecuteStatement(sql, {UUID})[1];

  -- Check if we have a result
  if (faction) then
    return faction[1];
  else
    return nil;
  end
end

function CheckExistingFaction(Name)
  local sql = "SELECT faction_id FROM factions WHERE LOWER(faction_name) = LOWER(?)";
  local faction = ExecuteStatement(sql, {Name})[1];

  if (faction) then
    -- Faction name is already taken
    return true;
  else
    -- Faction name is free
    return false;
  end
end

function GetPlayerFactionRank(UUID)
  -- Get a list of all the ranks the player has
  local sql = "SELECT faction_rank FROM faction_members WHERE player_uuid = ?";
  local faction_rank_list = ExecuteStatement(sql, {UUID});

  -- Check if there are any ranks at all
  if not (faction_rank_list) then
    -- There are no ranks at all
    return nil;
  else
    -- Ranks have been found
    -- Check if the player has the master rank
    local rank = nil;
    for key, value in pairs(faction_rank_list) do
      -- Check if the rank is nil or whether the rank is higher than previous found
      if (rank == nil) or (FactionRanks[value[1]] > FactionRanks[rank]) then
        -- Rank is nil or the rank is higher than the previous found
        rank = value[1];
      end
    end

    return rank;
  end
end

function GetFactionId(faction_name)
  -- Search for a faction by name
  local sql = "SELECT faction_id FROM factions WHERE LOWER(faction_name) = LOWER(?)";
  local faction_id = ExecuteStatement(sql, {faction_name})[1];

  -- Check if we have a result
  if (faction_id) then
    -- We have a result
    return faction_id[1];
  else
    -- No results
    return nil;
  end
end

function GetFactionName(faction_id)
  -- Search for a faction by id
  local sql = "SELECT faction_name FROM factions WHERE faction_id = ?";
  local faction_name = ExecuteStatement(sql, {faction_id})[1];

  -- Check if we have a result
  if (faction_name) then
    -- Found a faction
    return faction_name[1];
  else
    -- No faction has been found
    return nil;
  end
end