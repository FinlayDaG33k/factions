function ConfigureDatabase()
  -- Enable foreign keys
  local sqlSchema = {
    "PRAGMA foreign_keys = ON",
  };

  -- Load all the tables from the schema directory
  -- Order is important due to the foreign key constraints!
  local tables = {
    "factions",
    "players",
    "faction_members",
    "claims",
    "homes",
    "invitations"
  };

  -- loop over all the tables
  for i,table_name in ipairs(tables) do
    -- Check if the table already exists or not
    local table_exists = ExecuteStatement("SELECT * FROM sqlite_master WHERE name = ? AND type='table';",{table_name})[1];
    if not (table_exists) then
      -- Table does not exist yet, create it
      LOG("[" .. PLUGIN:GetName() .. "] Creating table \"" .. table_name .. "\"");

      -- Load the schema file
      local schema = PLUGIN:GetLocalFolder() .. cFile:GetPathSeparator() .. "schema" .. cFile:GetPathSeparator() .. table_name .. ".sql";
      
      -- Read the schema file
      local f = assert(io.open(schema, "rb"));
      local content = f:read("*all");
      f:close();

      -- Append the content to the sqlSchema variable
      table.insert(sqlSchema,content);
    end
  end

  for key in pairs(sqlSchema) do
    ExecuteStatement(sqlSchema[key]);
  end
end

function ExecuteStatement(sql, parameters)
	local stmt = db:prepare(sql);
	local result;

	if not (parameters == nil) then
		for key, value in pairs(parameters) do
			stmt:bind(key, value);
		end
	end

	local result = {};
	if (sql:match("SELECT")) then
		local x = 1;
		while (stmt:step() == sqlite3.ROW) do
			result[x] = stmt:get_values();
			x = x + 1;
		end
	else
		stmt:step();
	end

	stmt:finalize();

	if (sql:match("INSERT")) then
		result = db:last_insert_rowid();
	end

	if not (result == nil) then
		return result;
	else
		return 0;
	end
end