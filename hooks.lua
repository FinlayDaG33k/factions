function OnPlayerJoined(Player)
  local sql = "INSERT OR IGNORE INTO players (player_uuid, player_name, first_joined) VALUES (?, ?, DATETIME(\"now\"))";
  if(ExecuteStatement(sql,{Player:GetUUID(), Player:GetName()}) == nil) then
    LOG("[" .. PLUGIN:GetName() .. "] Couldn't add player " .. Player:GetName() .. " to the database!");
  end

  local sql = "UPDATE players SET last_online = NULL WHERE player_uuid = ?";
  ExecuteStatement(sql, {Player:GetUUID()});
end

function OnPlayerLeaving(Player)
  -- Set the last_online
  local sql = "UPDATE players SET last_online = datetime(\"now\") WHERE player_uuid = ?";
  ExecuteStatement(sql , {Player:GetUUID()});

  return false;
end