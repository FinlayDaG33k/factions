PLUGIN = nil
database_name = "database.sqlite3"

function Initialize(Plugin)
  -- Start initialization
  PLUGIN = Plugin;
  PLUGIN:SetName("Factions");
  PLUGIN:SetVersion(0);
  LOG("[" .. PLUGIN:GetName() .. "] Initializing v." .. PLUGIN:GetVersion());

  -- Load the Info shared library
  dofile(cPluginManager:GetPluginsPath() .. "/InfoReg.lua");

  -- Create a new Database file if this is the first load, else, open the existing one
  if not (cFile:IsFile(PLUGIN:GetLocalFolder() .. cFile:GetPathSeparator() .. database_name)) then -- If true, means database is deleted, or the plugin runs for the first time
    LOG("[" .. PLUGIN:GetName() .. "] It looks like this is the first time running this plugin. Creating database")
  else
    LOG("[" .. PLUGIN:GetName() .. "] Opening existing database");
  end
  db = sqlite3.open(PLUGIN:GetLocalFolder() .. cFile:GetPathSeparator() .. database_name);
  ConfigureDatabase();
  

  -- Hooks
  LOG("[" .. PLUGIN:GetName() .. "] Enabling hooks");
  cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_JOINED, OnPlayerJoined);
  cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_DESTROYED, OnPlayerLeaving);

  -- Command Bindings
  LOG("[" .. PLUGIN:GetName() .. "] Binding commands");
  RegisterPluginInfoCommands();

  -- Initializing is complete!
  LOG("[" .. PLUGIN:GetName() .. "] Initializing complete!");
  return true
end

function OnDisable()
  LOG("[" .. PLUGIN:GetName() .. "] Deactivating!");
end