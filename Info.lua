g_PluginInfo = {
	Name = "Factions",
	Date = "2018-12-23",
	Description = "An open-source faction plugin",

	-- The following members will be documented in greater detail later:
	AdditionalInfo = {
    {
      Title = "What is Factions?",
      Contents = ""
    },
  },
	Commands = {
    ["/faction"] = {
      Alias = "/f",
      Subcommands = {
        create = {
          Handler = FactionCreate,
          HelpString = "Creates a new faction",
          Permission = "factions.faction.create"
        },
        disband = {
          Handler = FactionDisband,
          HelpString = "Disbands your faction (only the master can do this!)",
          Permission = "factions.faction.disband"
        },
        invite = {
          Handler = FactionInvite,
          HelpString = "Invite a player to your faction",
          Permission = "factions.faction.invite"
        },
        join = {
          Handler = FactionJoin,
          HelpString = "Join a faction",
          Permission = "factions.faction.join"
        },
        sethome = {
          Handler = FactionSethome,
          HelpString = "Set a home for your faction",
          Permission = "factions.faction.home.set"
        },
        home = {
          Handler = FactionHome,
          HelpString = "Go to your faction home",
          Permission = "factions.faction.home"
        }
      }
    }
  },
	ConsoleCommands = {},
	Permissions = {
    ["factions.faction.create"] = {
      Description = "Allows the player to create a new faction",
      RecommendedGroups = "default"
    }
  },
	Categories = {},
}