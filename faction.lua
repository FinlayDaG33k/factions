Disbanding = {};

function FactionCreate(Split, Player)
  -- Make sure the player entered a faction name
  if (Split[3] == nil) then
		Player:SendMessageFailure("You have to enter a faction name! Usage: /faction create <name>");
		return true;
  end
  
  -- Check whether the player is already a member of a faction
  if (GetPlayerFaction(Player:GetUUID())) then
    -- Player already is a member of a faction
    Player:SendMessageFailure("You can not create a faction if you're already a member of another faction!");
  else
    -- Player is not a member of a faction
    -- Check if the faction name is already taken
    if (CheckExistingFaction(Split[3])) then
      -- Faction name is already taken
      Player:SendMessageFailure("The faction name " .. Split[3] .. " has already been taken");
    else
      -- Faction name is free
      -- Insert the faction into the table
      local sql = "INSERT INTO factions (faction_name, faction_founder) VALUES (?, ?)";
      local faction_id = ExecuteStatement(sql, {Split[3], Player:GetUUID()});

      -- Add the user as a member of the faction
      local sql = "INSERT INTO faction_members (player_uuid, faction_id, faction_rank) VALUES (?, ?, ?)";
      ExecuteStatement(sql, {Player:GetUUID(), faction_id, 'member'}); -- Add member rank to make sure the player can't be kicked by removing the rank
      ExecuteStatement(sql, {Player:GetUUID(), faction_id, 'master'}); -- Add master rank

      Player:SendMessageSuccess("Faction " .. Split[3] .. " has now been formed!");
    end
  end

  return true;
end

function FactionDisband(Split, Player)
  local player_faction = {
    GetPlayerFaction(Player:GetUUID()),
    GetPlayerFactionRank(Player:GetUUID())
  };

  if (Disbanding[Player:GetUUID()]) then
    -- Player confirmed disbanding
    -- Get the details for the faction
    local sql = "SELECT faction_name FROM factions where faction_id = ?";
    local faction_name = ExecuteStatement(sql, {Disbanding[Player:GetUUID()]})[1][1];

    -- Delete the faction
    local sql = "DELETE FROM factions WHERE faction_id = ?";
    ExecuteStatement(sql, {Disbanding[Player:GetUUID()]});

    -- Finish up
    Disbanding[Player:GetUUID()] = nil;
    Player:SendMessageSuccess(faction_name .. " has been disbanded!");

    return true;
  else
    -- Player is entering disbanding mode
    local faction_target;

    -- Check if the player specified a specific faction
    if (Split[3]) then
      -- A specific faction has been specified
      -- Load it's data from the database
      local sql = "SELECT faction_id, faction_name FROM factions WHERE faction_name = ?";
      faction_target = ExecuteStatement(sql, {Split[3]})[1];

      -- Check if a town has been found
      if not (faction_target) then
        Player:SendMessageFailure("That faction does not exist");
        return true;
      end
    else
      -- No specific faction has been specified
      -- Select the faction the user is in
      local sql = "SELECT faction_id, faction_name FROM factions WHERE faction_id = ?";
      faction_target = ExecuteStatement(sql, {player_faction[1]})[1];
    end

    -- Check if the player is a member of the faction
    if (player_faction[1] == faction_target[1]) then
      -- Player is a member of the faction
      -- Check if the player the master of the faction
      if (FactionRanks[GetPlayerFactionRank(Player:GetUUID())] == FactionRanks['master']) then
        -- Player is the master of the faction
        -- Ask for confirmation
        Disbanding[Player:GetUUID()] = faction_target[1];
        Player:SendMessageInfo("Are you sure you want to disband " .. faction_target[2] .. "?");
        Player:SendMessageInfo("This action can not be undone!");
        Player:SendMessageInfo("Use `/faction disband` again if you wish to continue.");
      else
        -- Player is not the master of the faction
        Player:SendMessageFailure("Only the master can disband your faction!");
      end
    else
      -- Player is not a member of the faction
      -- Check if the player has the permission to disband other factions
      if (Player:HasPermission("factions.faction.disband.other")) then
        -- Player has the permission to disband other factions
        Disbanding[Player:GetUUID()] = faction_target[1];
        Player:SendMessageInfo("Are you sure you want to disband " .. faction_target[2] .. "?");
        Player:SendMessageInfo("This action can not be undone!")
        Player:SendMessageInfo("Use `/faction disband` again if you wish to continue.");
      else
        -- Player does not have the permission to disband other factions
        Player:SendMessageFailure("You are not allowed to disband other factions!");
      end
    end
  end
  
  return true;
end

function FactionInvite(Split, Player)
  -- Check if a playername has been specified
  if(Split[3] == nil) then
    -- No playername has been specified
    Player:SendMessageFailure("Please specify a player to invite");

    return true;
  end

  -- Get the faction details for the inviting player
  local player_faction = {
    GetPlayerFaction(Player:GetUUID()),
    GetPlayerFactionRank(Player:GetUUID())
  };

  -- Check if inviting player is in a faction
  if (player_faction[1]) then
    -- Player is in a faction
    -- Check if the player is atleast of the member rank
    if (FactionRanks[player_faction[2]] >= FactionRanks['member']) then
      -- Player is atleast of the member rank
      -- Get the UUID for the invited player
      local UUID_target = cMojangAPI:GetUUIDFromPlayerName(Split[3]);

      -- Get the faction details for the invited player
      local faction_target = {
        GetPlayerFaction(UUID_target)
      };

      -- Check if the invited player is already in a faction or not
      if (faction_target[1]) then
        -- Player already belongs to a faction
        -- Check if player belongs to the same faction
        if (player_faction[1] == faction_target[1]) then
          -- Player belongs to the same faction
          Player:SendMessageFailure("Player already belongs to your faction");
        else
          -- Player belongs to a different faction
          Player:SendMessageFailure("Player already belongs to another faction");
        end
      else
        -- Player does not belong to a faction yet
        -- Check if the player is already invited to the faction
        local sql = "SELECT * FROM invitations WHERE player_uuid = ? AND faction_id = ?";
        local result = ExecuteStatement(sql, {UUID_target, player_faction[1]})[1];
        if (result) then
          -- Player is already invited to the faction
          Player:SendMessageFailure(Split[3] .. " is already invited to your faction");
        else
          -- Player is not invited to the faction yet
          -- "Send" invite
          local sql = "INSERT INTO invitations (player_uuid, faction_id) VALUES (?, ?)";
          ExecuteStatement(sql, {UUID_target, player_faction[1]});

          Player:SendMessageSuccess(Split[3] .. " is successfully invited to your faction");
        end
      end
    else
      -- Player is too low ranked
      Player:SendMessageFailure("You have to be of a higher rank to invite players");
    end
  else
    -- Player is not in a faction
    Player:SendMessageFailure("You have to be in a faction to invite players");
  end

  return true;
end

function FactionJoin(Split, Player)
  -- Check if there are any invitations
  local sql = "SELECT faction_id FROM invitations WHERE player_uuid = ?";
  local invitations = ExecuteStatement(sql, {Player:GetUUID()});
  if (invitations[1]) then
    -- Player has invitations
    local faction_id;

    -- Check if the player specifies which faction he wants to join
    if (Split[3]) then
      -- Player specified which faction he wants to join
      faction_id = GetFactionId(Split[3]);
    else
      -- Player did not specify the faction he wants to join
      -- Check if the player has more than one invite
      if (invitations[2]) then
        -- Player has more than one invite
        Player:SendMessageInfo("You have multiple invitations, please specify which faction you want to join:");
      
        -- Loop over the invites
        for key, value in pairs(invitations) do
          -- Display the faction name
          Player:SendMessageInfo(GetFactionName(value[1]));
        end
        
        return true;
      else
        -- Player only has one invitation
        faction_id = GetFactionId(Split[3]);
      end
    end

    -- Make invited player a member of the faction
    local sql = "INSERT INTO faction_members (player_uuid, faction_id, faction_rank) VALUES (?, ?, ?)";
    ExecuteStatement(sql, {Player:GetUUID(), faction_id, 'member'});

    -- Delete all invitation for the player
    local sql = "DELETE FROM invitations WHERE player_uuid = ?";
    ExecuteStatement(sql, {Player:GetUUID()});

    -- Inform the player he is not a member
    Player:SendMessageSuccess("You succesfully joined " .. GetFactionName(faction_id) .. "!");
  else
    -- There are no invitations
    Player:SendMessageFailure("You have no invitations!");
  end

  return true;
end

function FactionSethome(Split, Player)
  -- Get the player's faction details
  local player_faction = {
    GetPlayerFaction(Player:GetUUID()),
    GetPlayerFactionRank(Player:GetUUID())
  };

  -- Check if the player is in a faction
  if (player_faction[1]) then
    -- Player is in a faction
    -- Check if the player is atleast an officer
    if (FactionRanks[player_faction[2]] >= FactionRanks['officer']) then
      -- Player is atleast an officer
      -- Insert the home into the database at 0,0,0
      local sql = "INSERT OR IGNORE INTO homes (faction_id, pos_x, pos_y, pos_z) VALUES (?, ?, ?, ?)";
      if (ExecuteStatement(sql,{player_faction[1], 0, 0, 0}) == nil) then
        LOG("[" .. PLUGIN:GetName() .. "] Couldn't add faction home to the database!");
      end

      -- Set the home to the player's position
      local sql = "UPDATE homes SET pos_x = ?, pos_y = ?, pos_z = ? WHERE faction_id = ?";
      ExecuteStatement(sql, {Player:GetPosX(), Player:GetPosY(), Player:GetPosZ(), player_faction[1]});
    
      Player:SendMessageSuccess("Your faction home has been set");
    else
      -- Player is too low ranked
      Player:SendMessageFailure("You have to be of a higher rank to do this");
    end
  else
    -- Player is not in a faction
    Player:SendMessageFailure("You have to be in a faction to do this");
  end

  return true;
end

function FactionHome(Split, Player)
  -- Get the player's faction details
  local player_faction = {
    GetPlayerFaction(Player:GetUUID()),
    GetPlayerFactionRank(Player:GetUUID())
  };

  -- Check if the player is in a faction
  if (player_faction[1]) then
    -- Player is in a faction
    -- Load the factions home
    local sql = "SELECT * FROM homes WHERE faction_id = ?";
    local faction_home = ExecuteStatement(sql, {player_faction[1]})[1];

    -- Check if the faction has a home
    if (faction_home) then
      -- Faction has a home
      -- Teleport the player
      Player:TeleportToCoords(faction_home[2], faction_home[3], faction_home[4]);
    else
      -- Faction has no home
      Player:SendMessageFailure("Your faction has no home set");
    end
  else
    -- Player is not in a faction
    Player:SendMessageFailure("You have to be in a faction to do this");
  end

  return true;
end