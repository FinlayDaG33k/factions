CREATE TABLE IF NOT EXISTS players (
  player_uuid STRING NOT NULL PRIMARY KEY,
  player_name STRING NOT NULL UNIQUE,
  last_online INTEGER,
  first_joined INTEGER
)