CREATE TABLE IF NOT EXISTS faction_members (
  player_uuid STRING NOT NULL,
  faction_id INTEGER NOT NULL,
  faction_rank STRING,
  PRIMARY KEY (player_uuid,faction_id,faction_rank),
  FOREIGN KEY (player_uuid) REFERENCES players(player_uuid) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (faction_id) REFERENCES factions(faction_id) ON UPDATE CASCADE ON DELETE CASCADE
)